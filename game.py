import numbers
from random import randint

player_name = input("Hi! What is your name? ")

for num in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)
    print("Guess", num, ":", player_name, "were you born in", month_number, "/", year_number, "?")
    answer = input("yes or no? ")
    if answer == "yes":
        print("I knew it!")
        exit()
    elif answer == "no" and num > 4:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")